# STAB CSPTA::Signin

CSPTA::Signin is an online terminal application that facilitates the checking-in of TAs via RFID/HID card swiping.

**Created by Eddy Chen '21**

**Dedicated to Johnny Lindbergh '18. I have kept on computering.**

## Setup
Before starting, ensure you have all prerequisites installed and a suitable platform on which to deploy the application. This application is designed to be run on some sort of computer terminal with web browsing capabilities; for example, a low-powered laptop, or a Raspberry Pi with a GUI and (touch)screen.

### Prerequisites
- MySQL/MariaDB instance
- [Letter Day](https://github.com/thomascastleman/but-what-letter-day-is-it-really) instance
- [TA Reminders](https://github.com/caatherton/TA-Reminders) instance

MySQL/MariaDB should be installed on the same platform you intend to deploy `CSPTA::Signin` on. Letter Day/TA Reminders could be installed on the same machine, but it is recommended to deploy them separately.

### Install `forever` globally
`forever` allows the application to be run constantly, restarting it every night or in event of failure. This needs to be installed globally like so:
```
$ [sudo] npm install forever -g
```

### Repo clone and environment setup
```bash
$ git clone https://bitbucket.org/echen_stab/cspta-signin.git
$ cd cspta-signin
$ git checkout v0.1.9 # NB: latest version at the time of this writing
$ npm install
$ exec ./version.sh
```

### Configure application
Copy `defaults/default_config.js` to `config.js` and make any necessary changes.

### Configure/initialize MySQL database
Copy `defaults/default_credentials.js` to `credentials.js` and fill in the placeholder fields with the details of the database user. The user should have full access to the database provided, else unexpected behavior may ensue.

Save the credentials secret and run the following command to initialize the database with the schema:

```bash
$ [sudo] mysql -u <your username here> -p < create_db.sql
```

After doing that, run `scripts/init_db.js` to populate the `users` table of the database. The `signins` and `records` table will be automatically populated by the application as it runs.

Note that the script should only be used for testing purposes, as it populates entries with random keycard IDs. The actual values must be entered into the `users` table prior to deployment. (It also doesn't help that the format of the keycard IDs is not known at this time, so it is assumed that they are strings.)

### Starting the application
Once everything is set up, simply start the application with the following command:
```
$ forever start ./server.js
```

If everything was configured correctly, you should be able to navigate to `localhost:42069` (by default) and see the sign-in screen.

## API Documentation
CSPTA::Signin provides an API for querying data stored in the non-volatile `records` table, which contains all past sign-in records. Note that anyone with a browser or REST client on the network can access this data, so this feature can be disabled if desired. Finer-grained access control will be implemented in a future version.

Each response will either include the requested data in the `data` attribute, or the error in the `err` attribute. Errors can be caused by a malformed query or database error; additional triggers can be found below.

Queries regarding TAs return something like this:
```json
{
    "data": [
        {
            "ta_uid": 42,
            "name": "Lorem Ipsum",
            "attendance": 0.696969696969697
        },
        {
            "ta_uid": 69,
            "name": "Dolor S. Amet",
            "attendance": 0.42
        }
    ]
}
```

And queries regarding records return responses that look something like this:
```json
{
    "data": [
        {
            "rec_date": "2019-12-04T05:00:00.000Z",
            "ta_uid": 6,
            "name": "John Doe",
            "isSignedIn": 1
        },
        {
            "rec_date": "2019-12-20T05:00:00.000Z",
            "ta_uid": 9,
            "name": "Jane Doe",
            "isSignedIn": 0
        }
    ]
}
```

While any erroneous query return looks something like this:
```json
{
    "err": "Unable to fetch requested records."
}
```

### GET `/api/allTAs`
Returns *all* registered TAs. Returns no additional errors.

### POST `/api/TAByName`
Returns the TA that matches the `name` provided. Returns an error if no TA exists with that name.

### POST `/api/TAByUID`
Returns the TA that matches the `ta_uid` provided. Returns an error if no TA exists with that UID.

### POST `/api/TAsLTattn`
Returns *only* TAs who have an attendance rating less than the `attn` provided. Returns an error if no TAs match the criterion.

### POST `/api/TAsGTattn`
Returns *only* TAs who have an attendance rating greater than the `attn` provided. Returns an error if no TAs match the criterion.

### GET `/api/allRecords`
Returns *all* records present in the `records` table if possible. Returns no additional errors.

### POST `/api/recordsByDate`
Returns *only* records created on the provided date, which should be a [moment](https://momentjs.com/)-parseable date string under the name `date`. Returns an error if no records were found for the given date.

### POST `/api/recordsByName`
Returns *only* records that match a TA with the specified `name`. Returns an error if no records were found for that TA.

### POST `/api/recordsByUID`
Returns *only* records that match a TA with the specified `ta_uid`. Returns an error if no records were found for that TA.

## Acknowledgements

- Hats off to Thomas Castleman '19 and Cole Atherton '21 for their hard work in developing the Letter Day and TA Reminders services, upon which this application relies strongly and without which would likely not have come to fruition at all. The repositories are linked above.
- My heartfelt thanks go to Mr. Zach Minster, whose help has been indispensible throughout not only the development process, but also my journey of becoming a more optimal computer scientist and a more mature human being.
