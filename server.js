// version declaration
var version = require('./version.js');

// imports
var moment = require('moment');
var express = require('express');
var request = require('request');
var mustacheExpress = require('mustache-express');

// application config
var db = require('./credentials.js');
var cfg = require('./config.js');

// webserver init/config
var app = express();
var routes = require('./routes.js').init(app);
app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

// start server
var server = app.listen(cfg.port, function() { // port is 42069 by default
    console.log(`CSPTA::Signin server version ${version} listening on port ${server.address().port} ${cfg.devmode ? '[DEVELOPER MODE]' : ''}${(cfg.debug ^ cfg.devmode) ? '[DEBUG MODE]' : ''}`);
});

// developer mode check
if (!cfg.devmode) {
    // back up yesterday's records if it was a school day
    request.post(cfg.letter_day_url + '/letterByDate', {json: true, body: {'date': moment().add(-1, 'd').format('Y-MM-DD')}}, function(err, res, body) {
        if (err) {
            throw new Error('Failed to fetch letter day data.');
        } else if (body && !body.err) {
            db.query('INSERT INTO records (ta_uid, rec_date, name, isSignedIn) SELECT ta_uid, DATE_ADD(CURDATE(), INTERVAL -1 DAY), name, isSignedIn FROM signins', function(err) {
                if (!err) {
                    db.query('TRUNCATE TABLE signins', function(err) {
                        if (!err) {
                            console.log('Signins table backed up and truncated.');
                            getAssignments();
                        } else {
                            throw new Error('Failed to truncate signins table.');
                        }
                    });
                } else {
                    throw new Error('Failed to back up signins table.');
                }
            });
        } else if (body && body.err) {
            console.log('Yesterday was not a school day, not backing up signins table.');
            getAssignments();
        }
    });
}

// get assignments from letter day
function getAssignments() {
    console.log(`Retrieving TA assignments for ${moment().format('MMMM Do, Y')}`)
    request(cfg.letter_day_url + '/letterToday', {json: true}, function(err, res, body) {
        if (!err && !body.err) {
            var numDay = body.data.letter.charCodeAt(0) - 65;
            request(cfg.ta_reminder_url + '/api/assignments', {json: true}, function(err, res, body) {
                if (!err) {
                    body.assignments[numDay].assignedTAs.forEach(function(item) {
                        db.query('INSERT INTO signins (ta_uid, name) VALUES (?, ?)', [item.taUID, item.name], function(err) {
                            if (!err) {
                                db.query('UPDATE users SET onduties = onduties + 1 WHERE ta_uid = ?', [item.taUID], function(err) {
                                    if (!err) {
                                        console.log(`Saved assignment for TA ${item.name}.`);
                                    } else {
                                        throw new Error('Failed to update users table.');
                                    }
                                });
                            } else {
                                throw new Error('Failed to update signins table.');
                            }
                        });
                    });
                } else {
                    throw new Error('Failed to fetch latest assignments.');
                }
            });
        } else {
            throw new Error('Failed to fetch letter day data.');
        }
    });
}

// force restart after one day
setTimeout(function() {
    console.log('Executing nightly restart.');
    process.exit();
}, 86400000);
