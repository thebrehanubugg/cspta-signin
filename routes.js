// imports
var moment = require('moment');
var jsonParser = require('body-parser').json();

// config
var db = require('./credentials.js');
var cfg = require('./config.js');

// API responses
var responses = {
    issue: 'of server-side issues. Please try again later.',
    err: 'Unable to fetch requested records.',
    nota: 'No TA(s) found for given',
    norec: 'No records found for given',
    inv: 'Invalid',
    np: 'not provided.'
}

module.exports = {
	// set up all routes
    init: function(app) {
        // homepage
        app.get('/', function(req, res) {
            res.render('index');
        });

        // sign-in page
        app.get('/signin', function(req, res) {
            res.render('signin');
        });

        // hall of fame page
        app.get('/hof', function(req, res) {
            db.query('SELECT name, signins, onduties FROM users WHERE onduties != 0', function(err, result) {
                if (!err) {
                    var resarr = [];
                    var full = 0;
                    result.forEach(function(item) {
                        resarr.push(item);
                        if ((item.signins / item.onduties) == 1) ++full;
                    });
                    resarr.sort((a, b) => ((b.signins / b.onduties) - (a.signins / a.onduties)));
                    resarr = resarr.slice(0, 3);
                    var resparr = [];
                    resarr.forEach((item) => resparr.push(item.name));
                    res.render('hof', {fullattn: full, topthree: resparr});
                } else {
                    res.render('hoferr', {debug: cfg.debug, stacktrace: err.message});
                }
            });
        });

        // auth handler
        app.post('/auth', jsonParser, function(req, res) {
            db.query('SELECT * FROM users WHERE card_id = ?', [req.body.card_id], function(err, result) {
                if (!err) {
                    db.query('SELECT * FROM signins WHERE ta_uid = ?', [result[0].ta_uid], function(err, result) {
                        if (result.length) {
                            db.query('SELECT * FROM signins WHERE ta_uid = ? AND isSignedIn = 0', [result[0].ta_uid], function(err, result) {
                                if (result.length) {
                                    db.query('UPDATE signins SET isSignedIn = 1 WHERE ta_uid = ?', [result[0].ta_uid], function(err) {
                                        if (!err) {
                                            db.query('UPDATE users SET signins = signins + 1 WHERE ta_uid = ?', [result[0].ta_uid], function(err) {
                                                if (!err) {
                                                    res.render(success);
                                                } else {
                                                    res.render('error', {errmsg: responses.issue, debug: cfg.debug, stacktrace: err.message});
                                                }
                                            });
                                        } else {
                                            res.render('error', {errmsg: responses.issue, debug: cfg.debug, stacktrace: err.message});
                                        }
                                    });
                                } else {
                                    res.render('error', {errmsg: 'you are already signed in.'});
                                }
                            });
                        } else {
                            res.render('error', {errmsg: 'you are not on duty today.'});
                        }
                    });
                } else {
                    res.render('error', {errmsg: 'you were not found in the TA database.'});
                }
            });
        });

        // api endpoints
        if (cfg.enable_api) { // enabled by default
            // helper function to convert from DB fields to attendance ratio
            function ta_f2r(temp, item) {
                temp.ta_uid = item.ta_uid;
                temp.name = item.name;
                temp.attendance = (item.signins / item.onduties);
            }

            // get all TAs
            app.get('/api/allTAs', function(req, res) {
                db.query('SELECT ta_uid, name, signins, onduties FROM users', function(err, result) {
                    if (!err) {
                        var resp = [];
                        result.forEach(function(item) {
                            var temp = {};
                            ta_f2r(temp, item);
                            resp.push(temp);
                        });
                        res.send({data: resp});
                    } else {
                        res.send({err: cfg.debug ? err.message : responses.err});
                    }
                });
            });

            // get TA given name
            app.post('/api/TAByName', jsonParser, function(req, res) {
                if (req.body.name) {
                    db.query('SELECT ta_uid, name, signins, onduties FROM users WHERE name = ?', [req.body.name], function(err, result) {
                        if (err) {
                            res.send({err: cfg.debug ? err.message : responses.err})
                        } else if (!result.length) {
                            res.send({err: `${responses.nota} name.`});
                        } else {
                            var resp = [];
                            result.forEach(function(item) {
                                var temp = {};
                                ta_f2r(temp, item);
                                resp.push(temp);
                            });
                            res.send({data: resp});
                        }
                    });
                } else {
                    res.send({err: `Name ${responses.np}`});
                }
            });

            // get TA given internal UID
            app.post('/api/TAByUID', jsonParser, function(req, res) {
                if (req.body.ta_uid) {
                    if (isNaN(parseInt(req.body.ta_uid))) {
                        res.send({err: `${responses.inv} UID.`});
                    } else {
                        db.query('SELECT ta_uid, name, signins, onduties FROM users WHERE ta_uid = ?', [parseInt(req.body.ta_uid)], function(err, result) {
                            if (err) {
                                res.send({err: cfg.debug ? err.message : responses.err})
                            } else if (!result.length) {
                                res.send({err: `${responses.nota} UID.`});
                            } else {
                                var resp = [];
                                result.forEach(function(item) {
                                    var temp = {};
                                    ta_f2r(temp, item);
                                    resp.push(temp);
                                });
                                res.send({data: resp});
                            }
                        });
                    }
                } else {
                    res.send({err: `UID ${responses.np}`});
                }
            });

            // get TAs less than a certain attendance rating
            app.post('/api/TAsLTattn', jsonParser, function(req, res) {
                if (req.body.attn) {
                    if (isNaN(parseFloat(req.body.attn))) {
                        res.send({err: `${responses.inv} attendance rating.`})
                    } else {
                        db.query('SELECT ta_uid, name, signins, onduties FROM users', function(err, result) {
                            if (err) {
                                res.send({err: cfg.debug ? err.message : responses.err})
                            } else if (!result.length) {
                                res.send({err: `${responses.nota} attendance rating.`});
                            } else {
                                var resp = [];
                                result.forEach(function(item) {
                                    var temp = {};
                                    ta_f2r(temp, item);
                                    if (temp.attendance < parseFloat(req.body.attn)) resp.push(temp);
                                });
                                res.send({data: resp});
                            }
                        });
                    }
                } else {
                    res.send({err: `Attendance rating ${responses.np}`});
                }
            });

            // get TAs greater than a certain attendance rating
            app.post('/api/TAsGTattn', jsonParser, function(req, res) {
                if (req.body.attn) {
                    if (isNaN(parseFloat(req.body.attn))) {
                        res.send({err: `${responses.inv} attendance rating.`})
                    } else {
                        db.query('SELECT ta_uid, name, signins, onduties FROM users', function(err, result) {
                            if (err) {
                                res.send({err: cfg.debug ? err.message : responses.err})
                            } else if (!result.length) {
                                res.send({err: `${responses.nota} attendance rating.`});
                            } else {
                                var resp = [];
                                result.forEach(function(item) {
                                    var temp = {};
                                    ta_f2r(temp, item);
                                    if (temp.attendance > parseFloat(req.body.attn)) resp.push(temp);
                                });
                                res.send({data: resp});
                            }
                        });
                    }
                } else {
                    res.send({err: `Attendance rating ${responses.np}`});
                }
            });
            
            // get all records
            app.get('/api/allRecords', function(req, res) {
                db.query('SELECT rec_date, ta_uid, name, isSignedIn FROM records', function(err, result) {
                    if (!err) {
                        var resp = [];
                        result.forEach(function(item) {
                            resp.push(item);
                        });
                        res.send({data: resp});
                    } else {
                        res.send({err: cfg.debug ? err.message : responses.err});
                    }
                });
            });

            // get records given date
            app.post('/api/recordsByDate', jsonParser, function(req, res) {
                if (req.body.date) {
                    var d = moment(req.body.date);
                    if (d.isValid()) {
                        db.query(`SELECT rec_date, ta_uid, name, isSignedIn FROM records WHERE rec_date = DATE(?)`, [d.format('Y-MM-DD')], function(err, result) {
                            if (err) {
                                res.send({err: cfg.debug ? err.message : responses.err});
                            } else if (!result.length) {
                                res.send({err: `${responses.norec} date.`});
                            } else {
                                var resp = [];
                                result.forEach(function(item) {
                                    resp.push(item);
                                });
                                res.send({data: resp});
                            }
                        });
                    } else {
                        res.send({err: `${responses.inv} date.`});
                    }
                } else {
                    res.send({err: `Date ${responses.np}`});
                }
            });

            // get records given TA name
            app.post('/api/recordsByName', jsonParser, function(req, res) {
                if (req.body.name) {
                    db.query('SELECT rec_date, ta_uid, name, isSignedIn FROM records WHERE name = ?', [req.body.name], function(err, result) {
                        if (err) {
                            res.send({err: cfg.debug ? err.message : responses.err})
                        } else if (!result.length) {
                            res.send({err: `${responses.norec} TA.`});
                        } else {
                            var resp = [];
                            result.forEach(function(item) {
                                resp.push(item);
                            });
                            res.send({data: resp});
                        }
                    });
                } else {
                    res.send({err: `Name ${responses.np}`});
                }
            });

            // get records given TA internal UID
            app.post('/api/recordsByUID', jsonParser, function(req, res) {
                if (req.body.ta_uid) {
                    if (isNaN(parseInt(req.body.ta_uid))) {
                        res.send({err: `${responses.inv} UID.`});
                    } else {
                        db.query('SELECT rec_date, ta_uid, name, isSignedIn FROM records WHERE ta_uid = ?', [parseInt(req.body.ta_uid)], function(err, result) {
                            if (err) {
                                res.send({err: cfg.debug ? err.message : responses.err})
                            } else if (!result.length) {
                                res.send({err: `${responses.norec} TA.`});
                            } else {
                                var resp = [];
                                result.forEach(function(item) {
                                    resp.push(item);
                                });
                                res.send({data: resp});
                            }
                        });
                    }
                } else {
                    res.send({err: `UID ${responses.np}`});
                }
            });
        } else {
            app.get('/api/*', function(req, res) {
                res.sendStatus(403);
            });
            app.post('/api/*', function(req, res) {
                res.sendStatus(403);
            });
        }
    }
}
