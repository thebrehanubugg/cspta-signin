DROP DATABASE IF EXISTS CSPTA_signin;
CREATE DATABASE CSPTA_signin;
USE CSPTA_signin;

-- user table
CREATE TABLE users (
    ta_uid INT NOT NULL,
    card_id VARCHAR(32) NOT NULL,
    name VARCHAR(128) UNIQUE NOT NULL,
    signins INT NOT NULL,
    onduties INT NOT NULL,
    PRIMARY KEY (ta_uid)
);

-- (today's) signin table
CREATE TABLE signins (
    ta_uid INT NOT NULL,
    name VARCHAR(128) UNIQUE NOT NULL,
    isSignedIn TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (ta_uid),
    FOREIGN KEY (ta_uid) REFERENCES users(ta_uid)
);

-- past signins
CREATE TABLE records (
    rec_id INT NOT NULL AUTO_INCREMENT,
    rec_date DATE NOT NULL,
    ta_uid INT NOT NULL,
    name VARCHAR(128) UNIQUE NOT NULL,
    isSignedIn TINYINT(1),
    PRIMARY KEY (rec_id)
);
