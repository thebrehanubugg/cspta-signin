#!/usr/bin/env bash
current="0.1.10"
stable="0.1.9"
gitrevs="`git rev-list v$stable..HEAD 2> /dev/null | wc -l`"
githash="`git rev-parse --short HEAD 2> /dev/null`"
file="version.js"

if [ "$gitrevs" -eq "0" ]; then
    gitrevs="-"
fi

if [ -n "$gitrevs" ] && [ "$gitrevs" != "-" ]; then
    gitrevs="-$gitrevs."
fi

if [ $current = $stable ]; then
    echo "module.exports = '$stable'" > $file
else
    echo "module.exports = '$current${gitrevs}git-$githash'" > $file
fi
