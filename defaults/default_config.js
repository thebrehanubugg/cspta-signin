/*
 * Default configuration file for CSPTA::Signin.
 * To use this, copy this to the same directory as `server.js` and rename it to `config.js`
 * then make any necessary changes.
 * Do not touch this directly lest you wish for merge conflicts while updating.
 */

module.exports = {
    /*
     * The port the application listens on.
     * If on Linux, port numbers lower than 1024 require root access.
     */
    port: 42069,
    /*
     * Whether the API is enabled.
     */
    enable_api: true,
    /* 
     * Whether debug mode is enabled.
     * When debug mode is on, erroneous sign-in attempts and
     * API calls will send the full stack trace instead of
     * (or in addition to) a user-friendly error.
     * Only enable this if you know what you are doing.
     */
    debug: false,
    /*
     * Whether developer mode is enabled.
     * When developer mode is on, the application will not attempt
     * to back up records or fetch new assignments on startup.
     * DO NOT ENABLE THIS ON PRODUCTION!
     */
    devmode: false,
    /*
     * The URL that points to your Letter Day instance.
     */
    letter_day_url: 'http://day.cs.stab.org',
    /*
     * The URL that points to your TA Reminders instance.
     */
    ta_reminder_url: 'http://hours.thelounge.sx'
}
