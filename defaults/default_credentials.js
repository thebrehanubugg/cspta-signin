/*
 * Boilerplate MySQL credentials secret for CSPTA::Signin.
 * To use this, copy this to the same directory as `server.js` and rename it to `credentials.js`
 * then make any necessary changes.
 * Do not touch this directly lest you wish for merge conflicts while updating.
 */

var mysql = require('mysql');

module.exports = mysql.createConnection({
    host: 'localhost',
    database: 'CSPTA_signin',
    // Only change the fields below.
    user: YOUR_USERNAME_HERE,
    password: YOUR_PASSWORD_HERE,
});
