#!/usr/bin/env node

var db = require('../credentials.js');
var req = require('request');

console.log('Initializing TA table with systematic keycard identifiers');
req('http://hours.thelounge.sx/api/assignments', {json: true}, function(err, res, body) {
    if (!err) {
        body.assignments.forEach(function(i) {
            i.assignedTAs.forEach(function(j) {
                db.query("INSERT IGNORE INTO users (ta_uid, card_id, name, signins, onduties) VALUES (?, ?, ?, 0, 0)", [j.taUID, j.name.split(' ')[0][0].toLowerCase() + j.name.split(' ')[1].toLowerCase(), j.name]);
            });
        });
    }
});

setTimeout(function() {
    process.exit();
}, 1000);
